console.log('Inicio do sistema de verificacao para votacao');

let idade:number = calcularIdade(1989, 2022);

console.log(idade);

if(idade >= 16){
    console.log('Já pode votar');
}else{
    console.log('Não pode votar');
}

function calcularIdade(anoInicial:number, anoFinal:number): number {
    return anoFinal - anoInicial;
}

// exemplos de classes
console.log('Exemplos de classes')

class Racao{
    nome?: string;
    preco?: number;

    imprimir(){
        console.log('Nome: ' + this.nome + ' Preco: ' + this.preco);    
        console.log(`Nome: ${this.nome} Preco: ${this.preco}`);    
    }
}

let r1: Racao = new Racao();
let r2: Racao = new Racao();

r1.nome = 'Racao Boa';
r2.nome = 'Racao Premium';

console.log(r1.nome);
console.log(r2.nome);

r1.imprimir();
r2.imprimir();
