"use strict";
console.log('Inicio do sistema de verificacao para votacao');
let idade = calcularIdade(1989, 2022);
console.log(idade);
if (idade >= 16) {
    console.log('Já pode votar');
}
else {
    console.log('Não pode votar');
}
function calcularIdade(anoInicial, anoFinal) {
    return anoFinal - anoInicial;
}
class Racao {
    imprimir() {
        console.log('Nome: ' + this.nome + ' Preco: ' + this.preco);
        console.log(`Nome: ${this.nome} Preco: ${this.preco}`);
    }
}
let r1 = new Racao();
let r2 = new Racao();
r1.nome = 'Racao Boa';
r2.nome = 'Racao Premium';
console.log(r1.nome);
console.log(r2.nome);
r1.imprimir();
r2.imprimir();
//# sourceMappingURL=index.js.map